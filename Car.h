#ifndef CAR_H_INCLUDED
#define CAR_H_INCLUDED
#include <string>
#include "Tire.h"

using namespace std;
const int NUMBER_OF_TIRES = 4;

class Car{
private:
    string model;
    string year;
    string type;
    Tire Tires[NUMBER_OF_TIRES];


public:
    Car();
    Car(string mod, string year, string typ);
    string getSpecs();
    void setSpecs( string mod, string yea, string typ);
    Tire getTireByIndex(int index);
    void setPressureByIndex(int index, int pres);

};

#endif // CAR_H_INCLUDED
