#include "Tire.h"

Tire::Tire()
{
    pressure = 35;
    specs = "R17";
}

Tire::Tire(int pre, string sp)
{
    pressure = pre;
    specs = sp;
}

int Tire::getPressure()
{
    return pressure;
}
void Tire::setPressure(int pre)
{
    pressure = pre;
}

string Tire::getSpecs()
{
    return specs;
}
void Tire::setSpecs( string sp)
{
    specs = sp;
}

