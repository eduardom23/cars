#ifndef TIRE_H_INCLUDED
#define TIRE_H_INCLUDED
#include <string>

using namespace std;

class Tire{
private:
    int pressure;
    string specs;

public:
    Tire();
    Tire(int pre, string sp);
    int getPressure();
    void setPressure(int pre);
    string getSpecs();
    void setSpecs( string sp);

};


#endif // TIRE_H_INCLUDED
